from django.test import TestCase
from django.test import Client
from django.urls import resolve
from hello.views import index

# Create your tests here.
class story8test(TestCase):
    def test_story8_url_exist(self):
        resp = Client().get('/')
        self.assertEqual(resp.status_code,200)
        self.assertTemplateUsed(resp,'index.html')
    def story8_index_function(self):
        find = resolve('/')
        self.assertEqual(find.func,index)
