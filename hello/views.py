from django.shortcuts import render

# Create your views here.
def index(request):
    context={'background':'background.jpg',}
    return render(request,'index.html',context)